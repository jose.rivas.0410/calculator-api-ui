import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  constructor(private http: HttpClient) { }

  multi(e: number, f:number) {
    return this.http.get(`${environment.url}/calculator/multiply/${e}/${f}`) as Observable<any>;
  }

  divide(g: number, h:number) {
    return this.http.get(`${environment.url}/calculator/divide/${g}/${h}`) as Observable<any>;
  }

  add(a: number, b:number) {
    return this.http.get(`${environment.url}/calculator/add/${a}/${b}`) as Observable<any>;
  }

  subtract(c: number, d:number) {
    return this.http.get(`${environment.url}/calculator/subtract/${c}/${d}`) as Observable<any>;
  }

  power(i: number) {
    return this.http.get(`${environment.url}/calculator/power/${i}`) as Observable<any>;
  }

  percent(j: number, k:number) {
    return this.http.get(`${environment.url}/calculator/percentage/${j}/${k}`) as Observable<any>;
  }

}
