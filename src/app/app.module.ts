import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Calculator2Component } from './components/calculator2/calculator2.component';
import { Calculator3Component } from './components/calculator3/calculator3.component';
import { Calculator4Component } from './components/calculator4/calculator4.component';
import { Calculator5Component } from './components/calculator5/calculator5.component';
import { Calculator6Component } from './components/calculator6/calculator6.component';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    Calculator2Component,
    Calculator3Component,
    Calculator4Component,
    Calculator5Component,
    Calculator6Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
