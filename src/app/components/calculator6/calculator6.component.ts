import { Component, OnInit } from '@angular/core';
import { FormControl, FormControlName, FormGroup } from '@angular/forms';
import { CalculatorService } from 'src/app/services/calculator.service';

@Component({
  selector: 'app-calculator6',
  templateUrl: './calculator6.component.html',
  styleUrls: ['./calculator6.component.css']
})
export class Calculator6Component implements OnInit {
  j: Number;
  k: Number;

  result: Number;

  percentForm = new FormGroup({
    j: new FormControl(''),
    k: new FormControl('')
  })

  constructor(private calSvc: CalculatorService) { }

  ngOnInit(): void {
  }

  percentNum(j, k) {
    this.result=null;
    this.calSvc.percent(j, k).toPromise().then((result) => {
      this.result = result;
    });
  }


}
