import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CalculatorService } from 'src/app/services/calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  a: Number;
  b: Number;

  result: number;

  addForm = new FormGroup({
    a: new FormControl(''),
    b: new FormControl('')
  });

  constructor(private calSvc: CalculatorService) { }

  ngOnInit(): void {
  }

  addNum(a, b) {
    this.result=null;
    this.calSvc.add(a, b)
    .toPromise()
    .then((result) => {
      // console.log(result);
      this.result = result;
    });
  }

}
