import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CalculatorService } from 'src/app/services/calculator.service';

@Component({
  selector: 'app-calculator4',
  templateUrl: './calculator4.component.html',
  styleUrls: ['./calculator4.component.css']
})
export class Calculator4Component implements OnInit {
  g: Number;
  h: Number;

  result: Number;

  divideForm = new FormGroup({
    g: new FormControl(''),
    h: new FormControl('')
  })

  constructor(private calSvc: CalculatorService) { }

  ngOnInit(): void {
  }

  divideNum(g, h) {
    this.result=null;
    this.calSvc.divide(g, h).toPromise().then((result) => {
      this.result = result;
    });
  }


}
