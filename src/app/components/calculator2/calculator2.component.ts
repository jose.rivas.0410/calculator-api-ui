import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CalculatorService } from 'src/app/services/calculator.service';

@Component({
  selector: 'app-calculator2',
  templateUrl: './calculator2.component.html',
  styleUrls: ['./calculator2.component.css']
})
export class Calculator2Component implements OnInit {
  c: Number;
  d: Number;

  minusForm = new FormGroup({
    c: new FormControl(''),
    d: new FormControl('')
  });
  
  result: number;

  constructor(private calSvc: CalculatorService) { }

  ngOnInit(): void {
  }

  minusNum(c, d) {
    this.result=null;
    this.calSvc.subtract(c, d)
    .toPromise()
    .then((result) => {
      this.result = result;
    });
  }

}
