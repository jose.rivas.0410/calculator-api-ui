import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CalculatorService } from 'src/app/services/calculator.service';

@Component({
  selector: 'app-calculator3',
  templateUrl: './calculator3.component.html',
  styleUrls: ['./calculator3.component.css']
})
export class Calculator3Component implements OnInit {
  e: Number;
  f: Number;

  multiForm = new FormGroup({
    e: new FormControl(''),
    f: new FormControl('')
  });

  result: Number;

  constructor(private calSvc: CalculatorService) { }

  ngOnInit(): void {
  }

  multiNum(e, f) {
    this.result=null;
    this.calSvc.multi(e, f).toPromise().then((result) => {
      this.result = result;
    });
  }

}
