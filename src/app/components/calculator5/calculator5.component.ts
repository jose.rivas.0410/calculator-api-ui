import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CalculatorService } from 'src/app/services/calculator.service';

@Component({
  selector: 'app-calculator5',
  templateUrl: './calculator5.component.html',
  styleUrls: ['./calculator5.component.css']
})
export class Calculator5Component implements OnInit {
  i: Number;

  result: Number;

  powerForm = new FormGroup({
    i: new FormControl('')
  })

  constructor(private calSvc: CalculatorService) { }

  ngOnInit(): void {
  }

  powerNum(i) {
    this.result=null;
    this.calSvc.power(i).toPromise().then((result) => {
      this.result = result;
    });
  }


}
